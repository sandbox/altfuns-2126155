<?php if ($render_drupport_wrapper): ?>
  <div id="drupport">
    <audio id="drupport-sound">
        <source src="<?php print $chat_message_sound_path; ?>.ogg" type="audio/ogg">
        <source src="<?php print $chat_message_sound_path; ?>.mp3" type="audio/mpeg">
    </audio>
    <div id="drupport-close-chat-yes" class="hidden"><?php print t('Close the chat') ?></div>
    <div id="drupport-close-chat-no" class="hidden"><?php print t('No') ?></div>
<?php endif; ?>
	<div id="<?php print $chat_guid ?>" class="drupport-chat">
		<div class="drupport-chat-header">
			<div>
				<a href="#" class="drupport-chat-close"></a> <a href="#" class="drupport-chat-minimize"></a>
			</div>
		</div>
		<div class="drupport-chat-content">
		  <?php if ($render_drupport_chat_ajax_wrapper): ?>
		    <div class="drupport-chat-ajax-wrapper">
		  <?php endif; ?>
		  <?php if ($chat_content != null): ?>
		    <?php print render($chat_content); ?>
		  <?php endif; ?>
		  <?php if ($render_drupport_chat_ajax_wrapper): ?>
		    </div>
		  <?php endif; ?>
		  <ul>
		    <?php foreach ($chat_messages as $chat_message): ?>
              <li>
                <b><?php print $chat_message['name'] ?>: </b><?php print $chat_message['message'] ?>
                <span class="time"><?php print $chat_message['timestamp'] ?></span>
              </li>
		    <?php endforeach; ?>
		  </ul>
		</div>
		<div class="drupport-chat-form">
			<?php print render($chat_form); ?>

			<div class="drupport-dialog-confirm">
             <p><span class="ui-icon ui-icon-alert"></span><?php print t("Do you want to close the chat?")?></p>
           </div>
		</div>
	</div>
<?php if ($render_drupport_wrapper): ?>
  </div>
<?php endif; ?>