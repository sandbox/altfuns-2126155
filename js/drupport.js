(function($) {
	Drupal.ajax = Drupal.ajax || {prototype : { commands : {}}};
	Drupal.Drupport = Drupal.Drupport || {};
	
    Drupal.behaviors.drupport = {
        'attach': function(context) {
        	if(Drupal.ajax['drupport-support-request-submit']){
        		// Overwrite beforeSubmit method of the request support form submit
                Drupal.ajax['drupport-support-request-submit'].options.beforeSubmit = function (form_values, element, options) {
                	var emailPattern = /^[a-zA-Z0-9._]+[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/;  
                	var formValid = true;
                	var nameField = $('#drupport-support-request-name');
                	var emailField = $('#drupport-support-request-email');
                	var questionField = $('#drupport-support-request-question');
                	
                	$('#drupport-support-request-form input, #drupport-support-request-form textarea').removeClass('error');
                	
                    // Check if the values of the form are empty or invalid
                	if (nameField.val() === '') {
                		nameField.addClass('error');
                		formValid = false;
                	}
                	
                	if (emailField.val() === '' || !emailPattern.test(emailField.val())) {
                		emailField.addClass('error');
                		formValid = false;
                	}
                	
                	if (questionField.val() === '') {
                		questionField.addClass('error');
                		formValid = false;
                	}
                	
                	return formValid;
                }	
        	}
            
        	// Bind the event to minimize chat
            $('.drupport-chat-minimize').live('click', function(e) {
    			var chatWrapper = $(this).closest('.drupport-chat');
    			var chatHeader = $('.drupport-chat-header', chatWrapper);
    			
    			chatWrapper.toggleClass('minimized');
    	
    			if (chatWrapper.hasClass('minimized')) {
    				chatWrapper.css({ top : chatWrapper.height() - chatHeader.height() });
    			} else {
    				chatWrapper.css({ top : 0 });
    			}
    		});
            
            // Bind the event to close chat
            $('.drupport-chat-close').live('click', function(e) {
            	var chatWrapper = $(this).closest('.drupport-chat');
            	var buttons = {};
            	
            	buttons[$('#drupport-close-chat-yes').text()] = 
            		function() {
            			$( this ).dialog( "close" );
            			chatWrapper.remove();
            			Drupal.Drupport.closeChat(chatWrapper.attr('id').replace('dc-', ''));
    	        	}
            
            	buttons[$('#drupport-close-chat-no').text()] =
            		function() {
            			$( this ).dialog( "close" );
      	        	}
            	
            	$(".drupport-dialog-confirm", chatWrapper).dialog({
            	      resizable: false,
            	      height:250,
            	      modal: true,
            	      buttons: buttons
            	    });
    		});
            
            // Set the local time of chat messages timestamp
            $('.drupport-chat-content ul li span.time').each(function() {
            	$(this).text(Drupal.Drupport.getLocalTime($(this).text()));
            });
            
            // Scroll down the chat messages
            $('.drupport-chat-content').each(function() {
            	$(this).scrollTop($(this)[0].scrollHeight);
            });
            
            // Buddy list user click event handler
            $('.drupport-chat-user.pending', context)
		            .addClass('active')
		            .live('click', function() {
		        // Retrieve the chat form from the drupport module backend.
		        $.get(Drupal.settings.basePath + 'drupport/support/start/' + this.id, null, supportStartCallback);
		        return false;
		    });
            
            // Buddy list user click event handler
            $('.form-item-drupport-message textarea', context)
		            .live('keyup', function(event) {
		        
			        if (event.which == 13) {
			        	if (this.value.trim().length > 0) {
				        	var form = $(this).closest('form');
				        	var guid = $('input[name=drupport_chat_guid]', form).val();
				        	var supporterId = $('input[name=drupport_supporter_id]', form);
				        	
				        	var data = {
					        		'message' : this.value,
					        		'chat_guid' : guid,
					        		'socket_id' : Drupal.Nodejs.socket.socket.sessionid
					        	};
				        	if (supporterId.length > 0){
				        		data.drupport_supporter_id = supporterId.val();
				        	}
				        	
					        // Retrieve the chat form from the drupport module backend.
					        $.post(Drupal.settings.basePath + 'drupport/message', data, messageCallback);
			        	}
				        
				        this.value = '';
			        }
		    });
        }
    }
    
    // Callback to process the response of the chat start request.
    var supportStartCallback = function(response) {
    	// Creates a new chat window.
        $('#drupport').append(response.data);
    }
    
    /**
	 * Ajax callback function to be called when a chat message has been posted.
	 */
    var messageCallback = function(response) {
    	
    }
    
    /**
	 * Ajax callback function to be called when the support request has been made.
	 */
	Drupal.ajax.prototype.commands.startChatCallback = function(ajax, response, status) {
		var chatTextArea = $('#edit-drupport-message');
		chatTextArea.closest('.form-type-textarea').removeClass('form-disabled');
		chatTextArea.removeAttr('disabled').removeClass('disabled');
	};
	
	/**
	 * NodeJS callback function to be executed after receiving a message.
	 */
	Drupal.Nodejs.callbacks.drupport = {
	  callback: function(message) {
		if(message.channel == 'supporters_channel'){
			Drupal.Drupport.notifySupporter($.parseJSON(message.data.body));
		}else{
			Drupal.Drupport.parsePushNotificationMessage($.parseJSON(message.data.body));	
		}
	  }
	};
    
    /**
	 * Function to parse the push notification message received.
	 * 
	 * @param message String with the given message
	 */
	Drupal.Drupport.parsePushNotificationMessage = function(message) {
		if(message.type == 'drupport_chat_status_change'){
			Drupal.Drupport.changeChatStatus(message);
		}else if(message.type == 'drupport_message'){
			Drupal.Drupport.processDrupportMessage(message);
		}	
	}
	
	/**
	 * Apends the message to the chat list and play the new message sound.
	 * @param message String with the given message
	 */
	Drupal.Drupport.processDrupportMessage = function(message) {
		var time = Drupal.Drupport.getLocalTime(message.timestamp);
		
		var chatContent = $('#dc-' + message.chatGuid + ' .drupport-chat-content');
		var chatHTMLMessage = '<li><b>' + message.transmitter + 
			': </b>' + message.message + '<span class="time">' + time + '</span></li>';
		var chatWaitMessage = $('p', chatContent);
		
		// Check if the chat has the wait message to remove it
		if (chatWaitMessage.length > 0) {
			chatWaitMessage.remove();
		}
		
		// Append the message
		$('ul', chatContent).append(chatHTMLMessage);
		
		// Check if the socket ID is different that the client one to emit the sound
		if (message.socketId != Drupal.Nodejs.socket.socket.sessionid){
			$('#drupport-sound')[0].play();
		}
		
		// Scroll the panel to the last message
		chatContent.scrollTop(chatContent[0].scrollHeight);
	}
	
	/**
	 * Change the status of the chat.
	 */
	Drupal.Drupport.changeChatStatus = function(message) {
		switch (message.status) {
		case 1: // Open
			break;
		case 2: // Closed
			// Disable the chat message field.
			var textarea = $('#dc-' + message.chatGuid + ' textarea[name=drupport_message]');
			if(textarea){
				textarea.attr("disabled", "disabled");
				textarea.addClass("disabled");
			}
			
			var date = new Date();
			date.setFullYear(1970);
			// Delete the chat cookie.
			document.cookie = 'Drupal_visitor_drupport_chat' + '=1;expires=' + date.toGMTString() + ';path=/;';
			break;
		default:
			break;
		}
	}
	
	/**
	 * Function to add the new chat user to buddy list.
	 * 
	 * @param message String with the given message
	 */
	Drupal.Drupport.notifySupporter = function(message) {
		if(message.type == 'drupport_chat_status_change'){
			var statusClass = '';
			var buddyListItem = $('li#' + message.chatGuid);
			switch (message.status) {
			case 1: // Open
				// Change the status of the chat.
				buddyListItem.removeClass('pending').addClass('open');
				$('.chat-state', buddyListItem).removeClass('pending').addClass('open');
				break;
			case 2: // Closed
				// Remove the list item when closed
				buddyListItem.remove();
				break;
			default:
				statusClass = 'open';
				break;
			}
			
		}else if(message.type == 'drupport_new_chat'){
			// Add the new chat to the buddy list.
			var listItem = '<li id="' + message.chatGuid + '" class="drupport-chat-user pending">' + message.userName + '</li>';
			var wrapper = $('#drupport-buddy-list-wrapper');
			var buddyList = $('#drupport-buddy-list-wrapper ul.drupport-buddy-list');
			buddyList.append(listItem);
			// Scroll the last to the last user.
			wrapper.scrollTop(wrapper[0].scrollHeight);
		}
		
	}
	
	/**
	 * Send to close the chat in the backend.
	 * 
	 * @param $chat_guid Unique identifier of the chat.
	 */
	Drupal.Drupport.closeChat = function(chat_guid) {
		var data = { 'chat_guid' : chat_guid };
    	
        // Close the chat in the backend.
        $.post(Drupal.settings.basePath + 'drupport/support/close', data, function(response){
        });
	}
	
	/**
	 * Function to format the timestamp in seconds to local time.
	 * 
	 * @param int with seconds
	 * 
	 * @return String with final local time format
	 */
	Drupal.Drupport.getLocalTime = function(seconds) {
		return new Date(seconds * 1000).toLocaleTimeString();
	}
})(jQuery);
