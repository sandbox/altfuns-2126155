<div id="drupport">
    <audio id="drupport-sound">
        <source src="<?php print $chat_message_sound_path; ?>.ogg" type="audio/ogg">
        <source src="<?php print $chat_message_sound_path; ?>.mp3" type="audio/mpeg">
    </audio>
    <div id="drupport-close-chat-yes" class="hidden"><?php print t('Close the chat') ?></div>
    <div id="drupport-close-chat-no" class="hidden"><?php print t('No') ?></div>
	<div class="drupport-chat drupport-buddylist">
		<div class="drupport-chat-header">
			<div class="drupport-chat-title">
				<?php print t('Buddy List') ?>
			</div>
			<a href="#" class="drupport-chat-minimize"></a>
		</div>
		<div id="drupport-buddy-list-wrapper" class="drupport-chat-content">
			<?php if ($chats != null && count($chats) > 0): ?>
			<ul class="drupport-buddy-list">
			<?php foreach($chats as $chat): ?>
				<li id="<?php print $chat['guid']?>" class="drupport-chat-user <?php print $chat['status'] == 1? 'open' : 'pending'?>"><?php print $chat['name']?>
				  <span class="chat-state <?php print $chat['status'] == 1? 'open' : 'pending'?>"></span></span>
				</li>
			<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
	</div>


</div>
