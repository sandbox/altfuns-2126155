<?php

/**
 * Method to generate the form for Drupport configuration.
 *
 * @see drupport_config_form_submit()
 */
function drupport_config_form($form, &$form_state) {
  // Messages fielset
  $form['messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Messages'),
    '#weight' => 0,
    '#collapsible' => true,
    '#collapsed' => false,
  );

  // No available supporters configuration message
  $form['messages']['no_available_supporters'] = array(
      '#type' => 'textarea',
      '#title' => t('"No available supporters" Message'),
      '#default_value' => variable_get('drupport_no_available_supporters_message', ''),
      '#description' => t('Include the message that will be displayed to '
           . 'the client when there are no available supporters'),
      '#required' => true,
  );

  // Chat request message
  $form['messages']['chat_request_message'] = array(
      '#type' => 'textarea',
      '#title' => t('"Chat request message" Message'),
      '#default_value' => variable_get('drupport_chat_request_message', ''),
      '#description' => t('Include the message that will be displayed to '
          . 'the client when the chat request has been sent.'),
      '#required' => true,
  );

  // Form submit button
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );

  return $form;
}

/**
 * Function to handle the configuration form submit.
 */
function drupport_config_form_submit($form, &$form_state) {
  variable_set('drupport_no_available_supporters_message', $form_state['values']['no_available_supporters']);
  variable_set('drupport_chat_request_message', $form_state['values']['chat_request_message']);

  drupal_set_message(t('Drupport configuration successfully saved.'));
}