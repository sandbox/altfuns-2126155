<?php

/**
 * Method to get the online supporter users.
 *
 * @param array $permissions_array Permission to search for
 *
 * @return Array with online supporter users
 */
function _get_online_supporter_users($permissions_array) {
  $online_supporters = array();

  $or = db_or()->condition('rp.permission', $permissions_array, 'IN')->condition('rp.rid', 2);

  // Get the online supporter users
  $datasets = db_select('sessions', 's')->distinct();;
  $datasets->leftJoin('users_roles', 'ur', 's.uid = ur.uid');
  $datasets->join('role_permission', 'rp', 'ur.rid = rp.rid OR rp.rid = 2');
  $datasets->join('users', 'u', 's.uid = u.uid');
  $datasets->condition('rp.permission', $permissions_array, 'IN');
  $datasets->condition('u.uid', 0, '!=');
  $datasets->fields('u', array('uid', 'name'));
  $query_result = $datasets->execute();

  // Parse the results
  while ($record = $query_result->fetchAssoc()) {
    $online_supporters[] = $record;
  }

  return $online_supporters;
}

/**
 * Method to get the chat visitor users.
 *
 * @return Array with chat visitors
 */
function _get_chat_visitors() {
  $chat_visitors = array();
  $status_filter = array(1, 3);

  // Get the chat visitors
  $datasets = db_select('drupport_chat', 'dc');
  $datasets->condition('dc.status', $status_filter, 'IN');
  $datasets->fields('dc', array('cid', 'name', 'status', 'guid'));

  $query_result = $datasets->execute();

  // Parse the results
  while ($record = $query_result->fetchAssoc()) {
    $chat_visitors[] = $record;
  }

  return $chat_visitors;
}

/**
 * Method to store a new chat in the database.
 *
 * @param Sting $guid GUID of the chat
 * @param String $name Name of the visitor
 * @param String $email Email of the visitor
 * @param String $question Question to start the chat
 */
function _record_chat($guid, $name, $email, $question) {
  db_insert('drupport_chat')
    ->fields(array(
      'name' => $name,
      'email' => $email,
      'question' => $question,
      'start_time' => time(),
      'status' => 3,  // Pending
      'guid' => $guid
    ))->execute();
}

/**
 * Get the chat object with the given guid.
 * @param string $chat_guid Unique identifier of the chat to retrieve
 * @return stdClass A object representing the chat. null if the chat is not found.
 */
function _get_chat($chat_guid){
 $datasets = db_select('drupport_chat', 'dc');
 $datasets->fields('dc', array('cid', 'uid', 'name'
   , 'email','question','start_time'
   , 'end_time', 'status', 'solution'));
 $datasets->condition('dc.guid', $chat_guid, '=');

 $query_result = $datasets->execute();

 $result_array = $query_result->fetchAssoc();

 $result = null;

 if($result_array){
  $result = new stdClass();
  // Convert the array to object.
  foreach ($result_array as $key => $value) {
   $result->$key = $value;
  }
 }

 return $result;
}

/**
 * Method to get the chat id from the chat guid.
 *
 * @param String $chat_guid GUID of the chat
 *
 * @return int with the chat id
 */
function _get_chat_id_from_guid($chat_guid) {
  $datasets = db_select('drupport_chat', 'dc');
  $datasets->condition('dc.guid', $chat_guid, '=');
  $datasets->fields('dc', array('cid'));

  $query_result = $datasets->execute();

  return $query_result->fetchAssoc();
}

/**
 * Method to record a chat message in the database.
 *
 * @param int $cid Id of the chat
 * @param int $uid Id of the supporter user or null
 * @param String $message Message to record
 * @param int Timestamp to use
 */
function _record_chat_message($cid, $uid, $message, $timestamp) {
  $mid = db_insert('drupport_chat_message')
    ->fields(array(
      'cid' => $cid,
      'uid' => $uid,
      'message' => $message,
      'timestamp' => $timestamp
    ))->execute();

  return $mid;
}

/**
 * Update the chat status
 * @param string $chat_guid Unique identifier of the chat that will be updated.
 * @param integer $chat_status The new status of the chat.
 */
function _update_chat_status($chat_guid, $chat_status, $uid = NULL){
  $statment = db_update('drupport_chat');
  $fields = array(
    'status' => $chat_status,
  );

  if(isset($uid)){
   $fields['uid'] = $uid;
  }
  
  $statment->fields($fields);
  
  $statment->condition('guid', $chat_guid, '=');
  return $statment->execute();
}

/**
 * Method to return the name of the visitor and supporter of a message.
 *
 * @param int $mid ID of the chat message
 *
 *  @return array with the given record from the database
 */
function _get_chat_message_transmitter($mid) {
  $datasets = db_select('drupport_chat', 'dc');
  $datasets->join('drupport_chat_message', 'dcm', 'dc.cid = dcm.cid');
  $datasets->leftJoin('users', 'u', 'dcm.uid = u.uid');
  $datasets->condition('dcm.mid', $mid, '=');
  $datasets->fields('dc', array('name'));
  $datasets->fields('u', array('name'));

  $query_result = $datasets->execute();

  $users = $query_result->fetchAssoc();

  // Check which user submitted the message
  if (isset($users['u_name'])) {
    return $users['u_name'];
  } else {
    return $users['name'];
  }
}

/**
 * Method to return the table with the chats history records from the database.
 *
 * @return Array of records with the chats history
 */
function _get_chats_history_table($supporter_uid = null) {
  $chats_history = array();

  $datasets = db_select('drupport_chat', 'dc');
  $datasets->join('users', 'u', 'dc.uid = u.uid');
  $datasets->fields('dc', array('cid', 'name', 'email', 'start_time', 'status'));
  $datasets->fields('u', array('uid', 'name'));

  // Check if the history is for a supporter or for all of them
  if ($supporter_uid != null) {
    $datasets->condition('u.uid', $supporter_uid, '=');
  }

  $datasets->orderBy('start_time', 'desc');

  $query_result = $datasets->execute();

  // Parse the results
  while ($record = $query_result->fetchAssoc()) {
    $chats_history[] = $record;
  }

  return $chats_history;
}

/**
 * Method to get the history of a chat
 *
 * @param String $chat_guid GUID of the chat
 *
 * @return Array with the history records of the chat
 */
function _get_chat_history($chat_guid, $use_cid = false) {
  $chat_history = array();

  $datasets = db_select('drupport_chat', 'dc');
  $datasets->join('drupport_chat_message', 'dcm', 'dc.cid = dcm.cid');
  $datasets->leftJoin('users', 'u', 'dcm.uid = u.uid');

  // Check which condition to use
  if ($use_cid) {
    $datasets->condition('dc.cid', $chat_guid, '=');
  } else {
    $datasets->condition('dc.guid', $chat_guid, '=');
  }

  $datasets->fields('dc', array('name'));
  $datasets->fields('u', array('name'));
  $datasets->fields('dcm', array('message', 'timestamp'));
  $datasets->orderBy('timestamp');

  $query_result = $datasets->execute();

  // Parse the results
  while ($record = $query_result->fetchAssoc()) {
    // Check the transmitter of the message
    if (isset($record['u_name'])) {
      $record['name'] = $record['u_name'];
    }

    $chat_history[] = $record;
  }

  return $chat_history;
}